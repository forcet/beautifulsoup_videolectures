# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__="Mario"
__date__ ="$23/07/2014 11:05:53 AM$"

import urllib
import json
import nltk
import simplejson as json
import csv
import re
 
 
def sparql(query, baseURL, format="application/json"):
	params={
		"default-graph": "",
		"should-sponge": "soft",
		"query": query,
		"debug": "on",
		"timeout": "",
		"format": format,
		"save": "display",
		"fname": ""
	}
	querypart=urllib.urlencode(params)
	response = urllib.urlopen(baseURL,querypart).read()
        
        return json.loads(response)


def consulta(palabra):
    query="""
    PREFIX  skos:    <http://www.w3.org/2004/02/skos/core#>
            select ?preferLabel ?broader ?concept
            from <http://data.utpl.edu.ec/dbpedia>
            where { ?sub skos:prefLabel ?preferLabel. 
                    ?sub skos:broader ?broader. 
                    ?sub ?concept skos:Concept.
            filter (regex(str(?sub),"%s","i")) 
            }limit 10
    """ % palabra 
    
    data=sparql(query, "http://apolo.utpl.edu.ec/vtitanio/sparql")
    
    return json.dumps(data, sort_keys=True, indent=4)

def tokens(texto):
    
    text = nltk.word_tokenize(texto)
    tagged = nltk.pos_tag(text)
    print(tagged)
    filtered = []
    for pair in tagged:
        if pair[1] in ['NN', 'NNP','NNS','VBG']:
            if len(pair[0]) > 3:
                filtered.append(pair[0])

    return filtered

def guardar(data):
    f = open('datos.csv', 'a')
    print (data)
    f.write(data+"\n")
    f.close()
    return "Datos Guardados"

if __name__ == "__main__":
    reader = csv.reader(open('datosVideoLectures.csv', 'rb'))
    for index,row in enumerate(reader):
        
        if (re.search("tiene_Titulo",row[0])):
            titulo = row[0].split(";")
            print titulo[2]
#    keyinput = raw_input("Ingresar: ")
            rango = tokens(titulo[2])
            print rango
            for num in range(len(rango)):
               load=consulta(rango[num]) 
               lista=json.loads(load)
               datos=(lista["results"])
               valor=(datos["bindings"])
               ran = len(valor)
               print ran
               for js in range(ran):
                    concepts=valor[js]
                    dat= (concepts["preferLabel"])
                    #print titulo[2]+";"+"related ;""concepto_"+str(js+1)
                    #print "concepto_"+str(js+1)+";"+dat["type"]+";"+dat["value"]
                    guardar(titulo[0]+";"+"related ;""concepto_"+str(js+1))
                    guardar("concepto_"+str(js+1)+";"+dat["type"].encode('ascii', 'ignore')+";"+dat["value"].encode('ascii', 'ignore'))
#            