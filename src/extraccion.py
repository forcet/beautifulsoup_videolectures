# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__="Mario"
__date__ ="$12/07/2014 10:14:46 AM$"

from bs4 import BeautifulSoup
import urllib2

base_url = "http://videolectures.net"
url_videos = "http://videolectures.net/Top/"
ajax = "http://videolectures.net/site/ajax/drilldown"

def soup(url):
    response = urllib2.urlopen(url)
    html = response.read()
    return BeautifulSoup(html)

def topicos():
    nombre = []
    cantidad = []
    html=soup(base_url)
    div = html.find("div", { "class" : "mod_body hoverable" })
    for li in div.find_all("li"):
        num = int(li.a.next_sibling.replace("(","").replace(")",""))
        menu_enlace= li.a.get("href")
        cantidad.append(num)
        nombre.append(menu_enlace)
                
    return (nombre), (cantidad)

def link_videos():
    lect = []
    link = topicos()
    cantidad = link[1]
    for nombres in range(len(link[0])):
        curso_url   = ''.join([base_url, link[0][nombres]])
        html        = soup(curso_url) 
        script        = html.find_all("script")
        valor= str(script[10]).split("'")
        i = valor[133].split("&")
        id = "?".join(i)
        link_videos= "/".join([ajax,id])
        
        lect.append(link_videos.encode('ascii', 'ignore'))
    return lect, cantidad

def videos_enlaces(enlace):
    pag = paginas(enlace)
    lect = []
    #curso_url   = ''.join([enlace, "&p="])
    for num in range(pag):
        curso_url   = ''.join([enlace, "&p="+str(num+1)])
        print curso_url
        html        = soup(curso_url)
        for div in html.find_all("div", { "class" : "lec_thumb" }):
            for a in div.findAll('a'):
                link = a.get("href")
                #print "".join([base_url,link])
                lect.append("".join([base_url,link]))
                #print link        
    return lect 

def paginas(enlace):
    html        = soup(enlace)
    div = html.find("div", { "class" : "paginator accent_bg" })
    for a in div.find_all("a"):
        hr = a.get("href")
        href = hr.split("(")
        link = href[1].split(")")
    return int(link[0])

def datos_videos(enlace):
    link = videos_enlaces(enlace)
    print len(link)
    cont = 0
    lect = []
    for cursos in range(len(link)):
        html = soup(link[cursos])
        if cont >= cont:
            lect.append([])
            tit=html.title.getText().encode('ascii',"ignore")
            titulo = tit.replace(" - VideoLectures.NET", "")
            
            lect[cont].append(":Curso_"+str(cursos+1)) 
            lect[cont].append(":tiene_Titulo") 
            lect[cont].append(titulo)

            lect[cont].append("\n:Curso_"+str(cursos+1)) 
            lect[cont].append(":tiene_Url") 
            lect[cont].append(link[cursos])
            
            descrpcion=html.find("div", { "class" : "wiki" })
            
            if not(descrpcion is None):
                lect[cont].append("\n:Curso_"+str(cursos+1)) 
                lect[cont].append(":tiene_Descripcion") 
                lect[cont].append(descrpcion.getText().replace("\n","").encode("ascii","ignore"))
            
            for also in html.find_all("div", { "id" : "vl_seealso" }):
                for p in also.find_all("p"):
                    archivos= p.getText().replace("\n","").replace("        ","").encode("ascii","ignore")
                    print archivos
                    if not(archivos.endswith("/help/")) or not (archivos.endswith("/aspx")):
                        lect[cont].append("\n:Curso_"+str(cursos+1))
                        lect[cont].append(":tiene_Archivos") 
                        lect[cont].append("".join([base_url,archivos]))
            
            
            for div in html.find_all("div", { "class" : "lec_data" }):
                for span in div.find_all("span"):
              
                    prop=span.string.split(":")
                    valor = span.next_sibling.string.replace("\n","").replace("&nbsp;","").encode("ascii","ignore") 

                    lect[cont].append("\n:Curso_"+str(cursos+1))                    
                    lect[cont].append(":tiene_"+prop[0].replace(" ","_"))
                    lect[cont].append(valor)
            
            guardar(lect)
        cont = cont +1
    return lect

def guardar(data):
   # f = open('datosVideoLectures.csv', 'a')
    lista = range(len(data))
    for i in lista:
        datos = ";".join(str(j.encode('ascii', 'ignore')) for j in data[i])       
    print (datos)
    #f.write(datos+"\n")
    #f.close()
    return "Datos Guardados"


if __name__ == "__main__":
    print "Extraccion de datos"    
    nombres = link_videos()
    lista = range(len(nombres[0]))
    for i in lista:
        nombre = nombres[0][i]
        print nombre
        data = datos_videos(nombre)
#    datos= link_videos()
#    print datos[0]
   # print datos_videos("http://videolectures.net/site/ajax/drilldown/?cid=318")
    #print topicos()
    #print soup('https://twitter.com/Forcet')
    
    
